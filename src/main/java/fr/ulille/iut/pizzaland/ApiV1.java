
package fr.ulille.iut.pizzaland;

import org.glassfish.jersey.server.ResourceConfig;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import jakarta.json.bind.JsonbBuilder;
import jakarta.ws.rs.ApplicationPath;

@SuppressWarnings("unused")
@ApplicationPath("api/v1/")
public class ApiV1 extends ResourceConfig {
    private static final Logger LOGGER = Logger.getLogger(ApiV1.class.getName());

    public ApiV1() {
        packages("fr.ulille.iut.pizzaland");
        String environment = System.getenv("PIZZAENV");

        if ( environment != null && environment.equals("withdb") ) {
            LOGGER.info("Loading with database");
            try {
                FileReader reader = new FileReader( getClass().getClassLoader().getResource("ingredients.json").getFile() );
                FileReader pReader = new FileReader( getClass().getClassLoader().getResource("pizzas.json").getFile() );
                List<Ingredient> ingredients = JsonbBuilder.create().fromJson(reader, new ArrayList<Ingredient>(){

					/**
					 * 
					 */
					private static final long serialVersionUID = -4485481340591256948L;}.getClass().getGenericSuperclass());
                List<Pizza> pizzas = JsonbBuilder.create().fromJson(pReader, new ArrayList<Pizza>(){

					/**
					 * 
					 */
					private static final long serialVersionUID = -6085899735117689479L;}.getClass().getGenericSuperclass());

                IngredientDao ingredientDao = BDDFactory.buildDao(IngredientDao.class);
                PizzaDao pizzaDao = BDDFactory.buildDao(PizzaDao.class);
                ingredientDao.dropTable();
                pizzaDao.dropPizzaTable();
                ingredientDao.createTable();
                pizzaDao.createPizzaTable();
                for ( Ingredient ingredient: ingredients) {
                        ingredientDao.insert(ingredient); 
                }
                for (Pizza pizza : pizzas) {
                	pizzaDao.insert(pizza);
                }
            } catch ( Exception ex ) {
                throw new IllegalStateException(ex);
            }
        } 
    }
}
