package fr.ulille.iut.pizzaland.dto;

import java.util.UUID;
import java.util.List;
import java.util.ArrayList;
import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
	private UUID id;
    private String name;
    private List<Ingredient> composition = new ArrayList<>();

    public PizzaDto() {
    }

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ingredient> getComposition() {
		return composition;
	}

	public void setComposition(List<Ingredient> composition) {
		this.composition = composition;
	}
}
