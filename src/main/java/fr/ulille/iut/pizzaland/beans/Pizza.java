package fr.ulille.iut.pizzaland.beans;

import java.util.UUID;
import java.util.List;
import java.util.ArrayList;

import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private UUID id = UUID.randomUUID();
    private String name;
    private List<Ingredient> composition = new ArrayList<>();
    
    public Pizza() {
    }

	public static PizzaDto toDto(Pizza pizza) {
		// TODO Auto-generated method stub
		return null;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto pizzaCreateDto) {
		// TODO Auto-generated method stub
		return null;
	}

	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Ingredient> getComposition() {
		return composition;
	}

	public void setComposition(List<Ingredient> composition) {
		this.composition = composition;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	public static Pizza fromDto(PizzaDto readEntity) {
		// TODO Auto-generated method stub
		return null;
	}

	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		// TODO Auto-generated method stub
		return null;
	}

}
